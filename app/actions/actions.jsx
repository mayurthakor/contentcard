//Action Generator
export var removeCard = (index) => {
  return{
    type:'REMOVE_CARD',
    index//Same as index:index
  }
};

export var getCardList = () => {
  return{
    type:'GET_CARD_LIST'
  }
};

//In ES6 just have to use export word which is same as below.
// module.exports = {
//   removeCard: removeCard
// }
