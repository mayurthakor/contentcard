//Our dedicated reducer to deal with Card array for delete item.
var nextCardId = 0,
    titleText = "Lorem ipsum ",
    descriptionText = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.";

var stateDefaults = {
  cardItemList: [
    {id:nextCardId++,src:"assets/img"+nextCardId+".png", appIcon: "assets/reacticon.png", title: titleText+nextCardId, description: descriptionText, active: true},
    {id:nextCardId++,src:"assets/img"+nextCardId+".png", appIcon: "assets/reacticon.png", title: titleText+nextCardId, description: descriptionText, active: true},
    {id:nextCardId++,src:"assets/img"+nextCardId+".png", appIcon: "assets/reacticon.png", title: titleText+nextCardId, description: descriptionText, active: true},
    {id:nextCardId++,src:"assets/img"+nextCardId+".png", appIcon: "assets/reacticon.png", title: titleText+nextCardId, description: descriptionText, active: true},
    {id:nextCardId++,src:"assets/img"+nextCardId+".png", appIcon: "assets/reacticon.png", title: titleText+nextCardId, description: descriptionText, active: true},
    {id:nextCardId++,src:"assets/img"+nextCardId+".png", appIcon: "assets/reacticon.png", title: titleText+nextCardId, description: descriptionText, active: true},
    {id:nextCardId++,src:"assets/img"+nextCardId+".png", appIcon: "assets/reacticon.png", title: titleText+nextCardId, description: descriptionText, active: true},
    {id:nextCardId++,src:"assets/img"+nextCardId+".png", appIcon: "assets/reacticon.png", title: titleText+nextCardId, description: descriptionText, active: true},
    {id:nextCardId++,src:"assets/img"+nextCardId+".png", appIcon: "assets/reacticon.png", title: titleText+nextCardId, description: descriptionText, active: true}
  ]
};

export var cardListReducer = (state = stateDefaults.cardItemList,action) => {
  switch(action.type){
    case 'GET_CARD_LIST':
      return state;
    case 'REMOVE_CARD':
      return state.filter((card) => card.id !== action.index);
    default:
      return state;
  }
}
