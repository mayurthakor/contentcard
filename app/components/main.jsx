import styles from '../css/app.css';

var React = require('react');
var Dashboard = require('Dashboard');

var Main = React.createClass({

  render: function(){
    return (
      <div className={styles.app}>
        <h3>Content Card component</h3>
        <Dashboard />
      </div>
    );
  }
})

module.exports = Main;
