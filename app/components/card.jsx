import styles from '../css/card.css';

var React = require('react');
var {connect} = require('react-redux');
var actions = require('actions');
var Col = require('react-bootstrap').Col;
var Thumbnail = require('react-bootstrap').Thumbnail;
var Button = require('react-bootstrap').Button;

var Card = React.createClass({

  render: function(){
      var {dispatch} = this.props;

      return(
        <Col xs={12} sm={6} md={4} lg={3} key={this.props.id}>
          <div className={styles.contentTile}>
            <div className={styles.cardImageContainer}>
              <Thumbnail src={this.props.src} className={styles.thumbStyle}>
                <div className={styles.cardActionOverlay}>
              		<Button onClick={()=>{
                    alert("Feature not ready!!!");
                  }}>Open</Button>
                  <Button onClick={() => {
                    dispatch(actions.removeCard(this.props.id));
                  }}>Delete</Button>
                  <Button onClick={()=>{
                    alert("Feature not ready!!!");
                  }}>Print</Button>
              	</div>
                <span><img src={this.props.appIcon} className={styles.appIcon}/><div className={styles.appTitle} >{this.props.title}</div></span>
                <p className={styles.appDescription}>{this.props.description}</p>
              </Thumbnail>
            </div>
          </div>
        </Col>
      );
  }
});

module.exports = connect()(Card);
