import styles from '../css/cardlist.css';

var React = require('react');
//Connect is companion of Provider component.
//Provider component is providing access to the storefor all of its children.
//But children still needs to specify which data they like.
var {connect} = require('react-redux');
var actions = require('actions');
var Card = require('Card');
var Grid = require('react-bootstrap').Grid;
var Row = require('react-bootstrap').Row;

var CardList = React.createClass({

  render: function(){
    var {dispatch} = this.props;
    var {cardItemList} = this.props;
    const cardItems = [];

  	for(let cardItem of cardItemList){
      cardItems.push(
        <Card key={cardItem.id}
              id={cardItem.id}
              src={cardItem.src}
              appIcon={cardItem.appIcon}
              title={cardItem.title}
              description={cardItem.description} />
        )
  	}

    return(
      <div>
          <Grid className={styles.cardList}>
            <Row>
                {cardItems}
            </Row>
          </Grid>
      </div>
    );
  }
});

module.exports = connect(
  (state) => {
    return{
        cardItemList: state.cardListActions
    };
  }
)(CardList);
