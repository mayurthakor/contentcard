var redux = require('redux');
var {cardListReducer} = require('reducers');

export var configure = () =>{

  var reducer = redux.combineReducers({
    cardListActions: cardListReducer
  });

  var store = redux.createStore(
    reducer
  );

  return store;
};
