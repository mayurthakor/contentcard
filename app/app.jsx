var React = require('react');
var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory} = require('react-router');
var {Provider}  = require('react-redux');

var actions = require('actions');
var store = require('configureStore').configure();

store.subscribe(() => {
  //console.log('New State',store.getState());
});

var Main = require('Main');

ReactDOM.render(
  // <Router history={hashHistory}>
  //     <Route path="/" component={Main}>
  //       <IndexRoute component={Dashboard} />
  //     </Route>
  // </Router>,
  <Provider store={store}>
    <Main />
  </Provider>,
  document.getElementById('app')
);
