# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A demo app using React.js
* Version 0.1

### How do I get set up? ###

*Prerequisites:- npm available on dev machine.

1. After cloning from GIT repo, go to the root folder from Terminal or command. 
2. run "npm install"
3. Once all the dependencies installed,run "webpack" or "webpack -w" (-w for continues detection of file changes).
4. A "bundle.js" should be build on successful execution of command.
5. run "node server.js". Application shall get hosted on "http://localhost:3000/"

### Currrent Status ###
1. Application run with a default set of 9 card tiles. These are static values feed in as an input array to the app.
2. Currently only "Delete" functionality is working. "Open" & "Print" are just for presentation.
3 No Routing or test cases added.